# To jest miejsce od którego należy zacząć zadanie TDD

# To jest miejsce od którego należy zacząć zadanie TDD
import pytest
from translator.ptranslator.gaderypoluki.gaderypoluki import GaDeRyPoLuKi
# musiałem rozwinąć ścieżki pakietów, program kompilował, ale testy nie lokalizowały modułów.
from pytest import fail


@pytest.fixture
def translator():
    translator = GaDeRyPoLuKi()
    return translator


@pytest.mark.skip
def test_spaces(translator):  ## to nie TDD
    # given
    msg = "lok lok"

    # when
    result = translator.translate(msg)

    # then
    assert result == "upi upi"  # ten test przechodzi


def test_reverse_translator(translator):  # czy translator odwraca przetworzony łańcuch znaków...
    # given
    msg = "lok"

    # when
    result = translator.translate(msg)

    # then
    assert result == "ipu"  # błąd testu
    # fail("TODO")


# noob test z odpalenia  translatora
def prezentacja(x):
    print(x)


moj_test = GaDeRyPoLuKi()
zmienna_testowa = input('wprowadź zmienną do translacji: ')

prezentacja(zmienna_testowa)
print()

prezentacja(moj_test.translate(zmienna_testowa))

prezentacja(moj_test.translate_ignore_case(zmienna_testowa))

prezentacja(moj_test.is_translatable(zmienna_testowa))

prezentacja(moj_test.get_code_length())

'''
test testu, 

@pytest.mark.skip
def test_TDD_spaces(translator):
    # given
    msg = "loklok"

    # when
    result = translator.translate(msg)

    # then
    assert result == "upiupi"

    ##test działa

'''