import pytest

from translator.ptranslator.gaderypoluki.gaderypoluki import GaDeRyPoLuKi
from pytest import fail

# zbiór testów w postaci skryptu pytest
#testad
# fixture, czyli wcześniej przygotowany kawałek kodu do wielokrotnego uzycia
@pytest.fixture
def translator():
    translator = GaDeRyPoLuKi()
    return translator

# test właściwy
@pytest.mark.skip
def test_test_should_translate(translator):
    # given
    msg = "lok"

    # when
    result = translator.translate(msg)

    # then
    assert result == "upi"



# uwaga na klauzulę wyłączającą test
@pytest.mark.skip
def test_should_stay_not_translated(translator):
    # given
    msg = "LOK"

    # when
    result = translator.translate(msg)

    # then
    assert result == "LOK"



@pytest.mark.skip
def test_should_translate2(translator):
    # given
    msg = "ala"

    # when
    result = translator.translate(msg)

    # then
    assert result == "gug"
#działą


def test_should_throw_exception(translator):
    # given

    # when
    with pytest.raises(Exception):
        translator.translate(None)

    # then



@pytest.mark.skip
def test_should_translate_ignore(translator):
    # given
    msg = "KOT"

    # when
    result = translator.translate_ignore_case(msg)

    # then
    assert result == "ipt"



def test_should_check_not_translatable(translator):
    # given
    c = "Z"

    # when
    result = translator.is_translatable(c)

    # then
    assert not result


@pytest.mark.skip
def test_should_check_translatable(translator):
    # given
    c = "g"

    # when
    result = translator.is_translatable(c)

    # then
    assert result == 1


#@pytest.mark.skip
def test_should_check_code_length(translator):
    # given

    # when
    size = translator.get_code_length()

    # then
    assert size == 12


