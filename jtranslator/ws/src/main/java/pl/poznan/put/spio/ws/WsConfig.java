package pl.poznan.put.spio.ws;

import lombok.RequiredArgsConstructor;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.poznan.put.spio.translator.GaDeRyPoLuKi;

import javax.xml.ws.Endpoint;

// prosta konfiguracja usługi WebService, z wykorzystaniem istniejącego translatora
@Configuration
@RequiredArgsConstructor
public class WsConfig {
    private final Bus bus;
    private final GaDeRyPoLuKi translator;

    @Bean
    public Endpoint endpoint() {
        EndpointImpl endpoint = new EndpointImpl(bus, new GaDeRyPoLuKiEndpoint(translator));
        endpoint.publish("/gaderypoluki");
        return endpoint;
    }
}
